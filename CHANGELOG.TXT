7.2.0
Major changes, links to YMovie removed

7.1.2
Trakt.tv fix

7.1.1
Access token changed

7.1.0
Support for chci-videt subpage

7.0.0
Changed SCC API

6.8.1
Typo fixed

6.8
CSFD - trakt link enhancement

6.7.1
Code clean up - TMDB

6.7
Bugfix in some TV shows on CSFD page (no series presented)

6.6
Basic support for Ymovie link in Trakt.tv

6.5
Basic support for Ymovie link in TMDB
Bugfix in CSFD episode page

6.4
Fixed rating CSFD

6.3
IMDB is no longer supported
code clean up

6.2
Bugfix - Trakt.tv

6.1
Bugfix - Webshare

6.0
Support for csfd.sk

5.9
changes in CSFD pages required changes in code for lists

5.8
bug fixes on Webshare page - logo placement

5.7
bug fixes on CSFD page

5.6
Code clean up

5.5
Webshare issues fixed

5.4
Code clean up

5.3
Added support for new CSFD design - home page movie lists

5.2
Added support for new CSFD design - search result

5.1
Added support for new CSFD design - lists

5.0
First working release for new CSFD design

4.9
First release for new csfd web

4.8
Webshare ident fix when URL ends with ident

4.7
TMDB type fix

4.6
VLC logo also on search page

4.5
Ident fix

4.4
Added VLC link to Webshare page

4.3
Fix for IMDB

4.2
Fix for trakt.tv

4.1
Hotfix - response data changed

4.0
Changes related to new SCC API 2.0

3.7
YMovie link changed

3.6
Name changed          
Links to update and icons fixed

3.5
Authorization changed
Repository moved to GitLab under SCC project group

3.4
YMovie link changed

3.3
Changed requests because of API change

3.2
Variable declaration
IMDB logo position fixed

3.1
Fixed problem with displaying audio information

3.0
Changed requests because of API change

2.9
Added SCC icons on TMDB page - currently only on movie/tv detail page
Changed description file

2.8
Icons are now located directly on github and used from there
Bug fixed on IMDB page

2.7
List logo channged

2.6
All logos changed to SCC

2.5
SCC logo in lists, thanks to SCC API just one request for the whole list

2.1
New logo SCC

2.0
Added YMovie link, more info:
https://ymovie.herokuapp.com/

1.9
Removed IMDB and Trakt because of high server load

1.8
Remove logo from list, waiting for endpoint

1.7
SC2 logo in list more simple

1.6
Add config where user can enable intergation on CSFD podrobne-vyhledavani

1.5
Fixed logo color on CSFD page based on rating

1.4
New logo for IMDB episode

1.3
Added SC2 logo and info text in episodes for CSFD and IMDB
Link from episode page on CSFD and IMDB leads to trakt.tv and not to the episode! Need to investigate.
Added info about new update URL

1.2
Correct file name

1.1
Fixed bug in Trakt.tv movie page
ToDo: display logo in lists
Small changes

1.0
Functional release for all services
ToDo: display logo in lists
logo changes

0.9
Almost real functional releas, working for all services
- csfd, imdb, trakt
Added media info to title attribute of SC2 logo
Added link from CSFD and IMDB to Trakt.tv
Fixed trakt ID error
New greay logo, autoupdate

0.5
First functional release, only for CSFD

0.3
Added basic check, not finished yet
Modified SC2 logo for CSFD - logo color depending on rating
code changes

0.2
Added IMDB and TRAKT integration, still no functionality!
Changes due to movies, seasons and episodes pages differences.

0.1
First test version - only adds SC2 logo to CSFD page, no real functionality!
